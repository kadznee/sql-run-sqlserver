﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using System.ComponentModel;

/* 
 * This class is part of http://scriptzrunner.codeplex.com/ 
 */

namespace ScriptRunner
{
    public enum MessageType
    {
        Error,
        SQLMessage,
        PassMessage,
        FailedMessage,
        Running,
        DataBaseConnectionFailure
    }

    public enum FileRunStatus
    {
        Passed,
        Failed,
        NotRun,
    }

    public class SQLManager
    {
        public ISqlServer server;
        public BackgroundWorker backgroundScriptWorker;
        public List<FilePathData> filePaths;
        public DispatcherTimer Timer;
        public DateTime StartTime;
        public FileInfo CurrentFile;
        public DateTime TotalTime;
        public string CurrentFileUid;
        public bool finished;
        public bool continueWait;
        public bool error;
        public bool fileOpened;
        public bool stopped;

        public SQLManager()
        {
            backgroundScriptWorker = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
            filePaths = new List<FilePathData>();
            Timer = new DispatcherTimer();
            continueWait = true;
        }

        public void InitServer(SqlServer server)
        {
            this.server = server;
            server.SetInfoMessageEvent(OnInfoMessage);
        }

        public void RunScripts()
        {
            backgroundScriptWorker.DoWork += RunScriptsStart;
            backgroundScriptWorker.RunWorkerCompleted += RunScriptsCompleted;
            backgroundScriptWorker.RunWorkerAsync();
        }

        private void RunScriptsCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            server.CloseServer();
            if(Timer.IsEnabled)
                Timer.Stop();
        }

        private void RunScriptsStart(object sender, DoWorkEventArgs e)
        {
            if (CheckServerAvailablity("logon", "password"))
            {
                stopped = false;
                finished = false;
                List<FileInfo> files = new List<FileInfo>();
                TotalTime = DateTime.Now;

                filePaths.ToList().ForEach(filepath =>
                {
                    FileInfo file = new FileInfo(filepath.fullFileName);
                    files.Add(file);
                });

                foreach (FileInfo file in files.ToList())
                {
                    CurrentFile = file;
                    CurrentFileUid = filePaths.FirstOrDefault(x => x.fullFileName == file.FullName).uid.ToString();
                    StartTime = DateTime.Now;

                    if (filePaths.FirstOrDefault(x => x.uid.ToString() == CurrentFileUid).fileRunStatus == FileRunStatus.NotRun)
                    {
                        string script = file.OpenText().ReadToEnd();

                        IEnumerable<string> commandStrings = Regex.Split(script, "^\\s*GO\\s*$", RegexOptions.Multiline);

                        error = false;

                        backgroundScriptWorker.ReportProgress(0, new MessageToReturn
                        {
                            fileName = CurrentFile != null ? CurrentFile.Name : "",
                            message = file.FullName + "................Running",
                            messageType = MessageType.Running,
                            fileUid = CurrentFileUid,
                        });
                        file.OpenText().Close();
 
                        Timer.Start();
                        foreach (string commandString in commandStrings.ToList().Where(commandString => commandString.Trim() != ""))
                        {
                            try
                            {
                                server.ExecuteScript(commandString);
                            }
                            catch
                            {
                                if (!stopped)
                                {
                                    backgroundScriptWorker.ReportProgress(0, new MessageToReturn
                                    {
                                        fileName = CurrentFile != null ? CurrentFile.Name : "",
                                        message = "No Database Connection!",
                                        messageType = MessageType.DataBaseConnectionFailure
                                    });
                                }

                                if (stopped)
                                {
                                    break;
                                }
                            }
                        }
                        Timer.Stop();

                        if (stopped)
                        {
                            break;
                        }

                        if (!error)
                        {
                            filePaths.FirstOrDefault(x => x.uid.ToString() == CurrentFileUid).fileRunStatus = FileRunStatus.Passed;
                            backgroundScriptWorker.ReportProgress(0, new MessageToReturn
                            {
                                fileName = CurrentFile != null ? CurrentFile.Name : "",
                                message = file.FullName + "...............Passed",
                                messageType = MessageType.PassMessage,
                                fileUid = CurrentFileUid,
                            });
                        }
                        else if (error)
                        {
                            filePaths.FirstOrDefault(x => x.uid.ToString() == CurrentFileUid).fileRunStatus = FileRunStatus.Failed;
                            backgroundScriptWorker.ReportProgress(0, new MessageToReturn
                            {
                                fileName = CurrentFile != null ? CurrentFile.Name : "",
                                message = file.FullName + "................Failed",
                                messageType = MessageType.FailedMessage,
                                fileUid = CurrentFileUid
                            });

                            do
                            {
                            } while (continueWait);
                            continueWait = true;
                           
                            if(error)
                                break;
                        }
                    }
                }

                finished = filePaths[filePaths.Count -1].uid.ToString() == CurrentFileUid;
            }
        }

        public void OnInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            MessageToReturn message = new MessageToReturn()
            {
                fileName = CurrentFile.Name
            };

            if (e.Errors[0].Class != 0)
            {
                error = true;
                message.message = " Script Failed: " + e.Errors[0].Message + " Line No:" + e.Errors[0].LineNumber;
                message.messageType = MessageType.Error;
                message.fileUid = CurrentFileUid;
            }
            else
            {
                 message.message = e.Message;
                 message.messageType = MessageType.SQLMessage;
                 message.fileUid = CurrentFileUid;
            }

            backgroundScriptWorker.ReportProgress(0, message);
        }

        private bool CheckServerAvailablity(string logon, string password)
        {
            try
            {
                server.PingServer();
                return true;
            }
            catch (Exception ex)
            {
                backgroundScriptWorker.ReportProgress(0, new MessageToReturn
                { 
                    fileName = CurrentFile != null ? CurrentFile.Name : "",
                    message = "No Database Connection!", 
                    messageType = MessageType.DataBaseConnectionFailure 
                });

                return false;
            }
        }

        public void StopProcessing()
        {
            Timer.Stop();
            stopped = true;
            server.KillServer();
            backgroundScriptWorker.Dispose();
            backgroundScriptWorker = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
        }
    }
}