﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScriptRunner
{
    public class FilePathData
    {
        public Guid uid;
        public string fullFileName;
        public string fileName()
        {
            if (!string.IsNullOrEmpty(fullFileName))
                return fullFileName.Remove(0, fullFileName.LastIndexOf("\\") + 1);

            return "";
        }

        public FileRunStatus fileRunStatus = FileRunStatus.NotRun;
    }

    public class MessageToReturn
    {
        public string fileUid;
        public string fileName;
        public string message;
        public MessageType messageType;
    }

    public enum BtnType
    {
        Run,
        ReRun,
        Stop,
        Resume
    }

}
