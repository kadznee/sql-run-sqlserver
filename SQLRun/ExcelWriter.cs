﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CarlosAg.ExcelXmlWriter;

namespace ScriptRunner
{
    public class ScriptDetails
    {
        public string uid;
        public string filename;
        public string timetaken;
        public string status;
        public List<string> remarks;
    }

    public interface IExcelWriter
    {
        void WriteToExcel(List<ScriptDetails> scriptDetails, string totalTime);
    }

    public class ExcelWriter : IExcelWriter
    {
        public void WriteToExcel(List<ScriptDetails> scriptDetails, string totalTime)
        {
            string datetime = DateTime.Now.ToString("dd_MM_yyyy_hh-mm-ss");
            string filenameXls = "ScriptReport_" + datetime + ".xls";
            string filenameCsv = "ScriptReport_" + datetime + ".csv";

            Workbook book = new Workbook();
            book.ExcelWorkbook.ActiveSheetIndex = 1;
            book.ExcelWorkbook.WindowTopX = 100;
            book.ExcelWorkbook.WindowTopY = 200;
            book.ExcelWorkbook.WindowHeight = 7000;
            book.ExcelWorkbook.WindowWidth = 8000;

            book.Properties.Title = "Script Run Details";
            book.Properties.Created = DateTime.Now;
            WorksheetStyle style = book.Styles.Add("HeaderStyle");
            style.Font.FontName = "Tahoma";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            style.Font.Color = "White";
            style.Interior.Color = "Blue";
            style.Interior.Pattern = StyleInteriorPattern.HorzStripe;

            style = book.Styles.Add("Default");
            style.Font.FontName = "Tahoma";
            style.Font.Size = 10;

            Worksheet sheet = book.Worksheets.Add("Script Data");
            sheet.Table.Columns.Add(new WorksheetColumn(200));
            sheet.Table.Columns.Add(new WorksheetColumn(100));
            sheet.Table.Columns.Add(new WorksheetColumn(100));
            sheet.Table.Columns.Add(new WorksheetColumn(1000));


            WorksheetRow row = sheet.Table.Rows.Add();
            row.Cells.Add(new WorksheetCell("File Name", "HeaderStyle"));
            row.Cells.Add(new WorksheetCell("Time Taken to Run", "HeaderStyle"));
            row.Cells.Add(new WorksheetCell("Status", "HeaderStyle"));
            row.Cells.Add(new WorksheetCell("Remarks", "HeaderStyle"));

           foreach(ScriptDetails scriptDetail in scriptDetails)
            {
                row = sheet.Table.Rows.Add();
                row.Cells.Add(scriptDetail.filename);
                row.Cells.Add(scriptDetail.timetaken);
                row.Cells.Add(scriptDetail.status);

                StringBuilder stringbuilder = new StringBuilder();
                scriptDetail.remarks.ForEach(y =>
                    {
                        stringbuilder.AppendLine(y);
                    });

                row.Cells.Add(stringbuilder.ToString());
            }

           row = sheet.Table.Rows.Add();
           row = sheet.Table.Rows.Add();
           row.Cells.Add("TotalTime");
           row.Cells.Add(totalTime);

            book.Save(filenameXls);

            try
            {
               System.Diagnostics.Process.Start(filenameXls);
            }
            catch {  }
        } 
    }
}
