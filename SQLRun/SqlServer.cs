﻿using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace ScriptRunner
{
    public interface ISqlServer
    {
        void CloseServer();
        void ExecuteScript(string commandString);
        ServerVersion PingServer();
        void SetInfoMessageEvent(SqlInfoMessageEventHandler OnInfoMessage);
        void KillServer();
    }

    public class SqlServer : ISqlServer
    {
        private readonly SqlConnection conn;
        private readonly Server serverConn;

        public SqlServer(string serverName, string userName, string password, string databaseName)
        {
            string connection = "Initial Catalog = "+ databaseName +"; Data Source =" + serverName + ";";

            if (string.IsNullOrEmpty(userName) && string.IsNullOrEmpty(password))
                connection += " Integrated Security=True";
            else if(!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                connection += "User ID=" + userName + ";password=" + password;

            connection += ";";

            conn = new SqlConnection(connection)
                       {
                           FireInfoMessageEventOnUserErrors = true
                       };

            serverConn = new Server(new ServerConnection(conn))
            {
                ConnectionContext = {
                                        StatementTimeout = 99999
                                    }
            };
        }

        public void CloseServer()
        {
            if (serverConn != null && serverConn.ConnectionContext.SqlConnectionObject.State != System.Data.ConnectionState.Closed)
                serverConn.ConnectionContext.SqlConnectionObject.Close();
        }

        public void ExecuteScript(string commandString)
        {
            serverConn.ConnectionContext.ExecuteWithResults(commandString);
        }

        public ServerVersion PingServer()
        {
            return serverConn.PingSqlServerVersion(serverConn.Name);
        }

        public void SetInfoMessageEvent(SqlInfoMessageEventHandler OnInfoMessage)
        {
            conn.InfoMessage += OnInfoMessage;
        }

        public void KillServer()
        {
            serverConn.ConnectionContext.Cancel();
            serverConn.ConnectionContext.Connect();
        }
    }
}