﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScriptRunner;
using System.IO;

namespace SQLRun
{
    public partial class Form1 : Form
    {
        private SQLManager sqlRun;
        private List<ScriptDetails> scriptDetails = new List<ScriptDetails>();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text.ToString() == BtnType.Run.ToString())
            {
                BtnRun_Click(false);
            }
            else if (button1.Text.ToString() == BtnType.ReRun.ToString())
            {
                InitialiseSqlManager(true, sqlRun.filePaths);
                InitialiseServerConnection();
                BtnReRun_Click();
            }
            else if (button1.Text.ToString() == BtnType.Stop.ToString())
            {
                BtnStop_Click();
            }
            else if (button1.Text.ToString() == BtnType.Resume.ToString())
            {
                button1.Text = BtnType.Stop.ToString();
                InitialiseSqlManager(true, sqlRun.filePaths);
                InitialiseServerConnection();
                BtnRun_Click(true);
            }
        }

        private void InitialiseServerConnection()
        {
            sqlRun.InitServer(new SqlServer(textBox1.Text, textBox3.Text, textBox4.Text, textBox2.Text));
        }

        private void InitialiseSqlManager(bool isReRunOrResume, List<FilePathData> renrunFilePath)
        {

            if (isReRunOrResume)
                renrunFilePath = sqlRun.filePaths;

            sqlRun = new SQLManager();

            if (renrunFilePath != null && renrunFilePath.Count > 0)
                sqlRun.filePaths = renrunFilePath.OrderBy(x => x.fullFileName).ToList();
        }

        private void BtnRun_Click(bool isReRun)
        {
            if (!isReRun)
            {
                if (sqlRun != null && sqlRun.filePaths.Count > 0)
                    InitialiseServerConnection();

                if (textBox5.Text != "" && Directory.Exists(textBox5.Text))
                {
                    listBox1.Items.Clear();
                    InitialiseSqlManager(isReRun, GetListOfFiles(Directory.GetFiles(textBox5.Text).ToList()));
                    InitialiseServerConnection();
                    AddFilesToList(sqlRun.filePaths, isReRun);
                }
            }

            if (sqlRun != null && sqlRun.filePaths.Count > 0) //stop run when click with no files
            {
                button1.Text = BtnType.Stop.ToString();
                button2.Enabled = false;

                sqlRun.backgroundScriptWorker.ProgressChanged += backgroundScriptWorker_ProgressChanged;
                sqlRun.Timer.Tick += WriteDurationTime;
                sqlRun.RunScripts();
            }
        }

        private void WriteDurationTime(object sender, EventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            dateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
            sqlRun.StartTime = new DateTime(sqlRun.StartTime.Year, sqlRun.StartTime.Month, sqlRun.StartTime.Day, sqlRun.StartTime.Hour, sqlRun.StartTime.Minute, sqlRun.StartTime.Second);
            label7.Text = dateTime.Subtract(sqlRun.StartTime).ToString();

            sqlRun.TotalTime = new DateTime(sqlRun.TotalTime.Year, sqlRun.TotalTime.Month, sqlRun.TotalTime.Day, sqlRun.TotalTime.Hour, sqlRun.TotalTime.Minute, sqlRun.TotalTime.Second);
            label8.Text = dateTime.Subtract(sqlRun.TotalTime).ToString();
        }

        private void WritePassedOrFailedMessageForReportGeneration(MessageToReturn message, bool isPassed)
        {
            if (!scriptDetails.Exists(x => x.uid == message.fileUid))
            {
                scriptDetails.Add(new ScriptDetails
                {
                    uid = message.fileUid ?? "",
                    filename = message.fileUid == null ? "" : sqlRun.filePaths.FirstOrDefault(y => y.uid.ToString() == message.fileUid).fileName(),
                    timetaken = label7.Text.ToString(),
                    status = isPassed ? "Passed" : "Failed",
                    remarks = isPassed ? new List<string>() : new List<string>() { message.message }
                });

            }
            else
            {
                if (message.messageType != MessageType.FailedMessage)
                {
                    if (scriptDetails.FirstOrDefault(x => x.uid == message.fileUid).remarks == null)
                        scriptDetails.FirstOrDefault(x => x.uid == message.fileUid).remarks = new List<string>() { message.message };
                    else
                        scriptDetails.FirstOrDefault(x => x.uid == message.fileUid).remarks.Add(message.message);
                }
            }
        }

        private void BindMessageToList(MessageToReturn message, Color color)
        {
            if (listBox1.Items.Count > 0)
            {
                //listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Uid == message.fileUid).Content = message.message;
                //listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Uid == message.fileUid).Foreground = new SolidColorBrush(color);
                //listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Uid == message.fileUid).Width = 730;
                //listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Uid == message.fileUid).Focus();
                listBox1.Items.Insert(0, new MyListBoxItem
                {
                    ItemColor = color,
                    Message = message.message.ToString()
                });
            }
        }

        public void backgroundScriptWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            MessageToReturn message = (MessageToReturn)e.UserState;

            if (message.messageType == MessageType.SQLMessage)
                listBox2.Items.Insert(0, message.message);
            else if (message.messageType == MessageType.Error)
            {
                listBox2.Items.Insert(0, new MyListBoxItem
                {
                    Message = message.message.ToString(),
                    ItemColor = Color.Red
                });

                WritePassedOrFailedMessageForReportGeneration(message, false);
            }
            else if (message.messageType == MessageType.DataBaseConnectionFailure)
            {
                listBox2.Items.Insert(0, new MyListBoxItem
                {
                    Message = message.message.ToString(),
                    ItemColor = Color.Red
                });

                WritePassedOrFailedMessageForReportGeneration(message, false);
            }
            else if (message.messageType == MessageType.PassMessage)
            {
                BindMessageToList(message, Color.DarkGreen);
                WritePassedOrFailedMessageForReportGeneration(message, true);
                label7.Text = "00:00:00";
            }
            else if (message.messageType == MessageType.FailedMessage)
            {
                BindMessageToList(message, Color.Red);
                WritePassedOrFailedMessageForReportGeneration(message, false);
                label7.Text = "00:00:00";

                if ((bool)!checkBox2.Checked)
                {
                    sqlRun.Timer.Stop();
                    label7.Text = "00:00:00";
                    button2.Enabled = true;
                    button1.Text = BtnType.Resume.ToString();
                }
                else
                    sqlRun.error = false;

                if (checkBox1.Checked != null)
                {
                    if ((bool)checkBox1.Checked && !sqlRun.fileOpened)
                    {
                        try
                        {
                            System.Diagnostics.Process.Start("Ssms.exe", sqlRun.filePaths.FirstOrDefault(y => y.uid.ToString() == message.fileUid).fullFileName);
                        }
                        catch
                        {
                            System.Diagnostics.Process.Start("notepad.exe", sqlRun.filePaths.FirstOrDefault(y => y.uid.ToString() == message.fileUid).fullFileName);
                        }

                        sqlRun.fileOpened = true;
                    }
                }

                sqlRun.continueWait = false;
            }
            else if (message.messageType == MessageType.Running)
            {
                BindMessageToList(message, Color.Orange);
            }

            if (sqlRun.finished)
            {
                button1.Text = BtnType.ReRun.ToString();
                button2.Enabled = true;
                sqlRun.finished = false;
            }
        }

        private void AddFilesToList(List<FilePathData> filepathDataList, bool isReRun)
        {
            filepathDataList.ForEach(x =>
            {
                if (isReRun)
                    x.fileRunStatus = FileRunStatus.NotRun;

                if (x.fullFileName.ToUpper().Contains(".SQL"))
                {
                    listBox1.Items.Add(new TextBox()
                    {
                        Tag = x.uid.ToString(),
                        Text = x.fullFileName
                    });
                }
            });
        }

        private static List<FilePathData> GetListOfFiles(List<String> strings)
        {
            List<FilePathData> listofString = new List<FilePathData>();

            strings.Sort();

            strings.ForEach(x => listofString.Add(new FilePathData
            {
                uid = Guid.NewGuid(),
                fullFileName = x
            }));

            return listofString;
        }

        private void Clear()
        {
            listBox2.Items.Clear();
            listBox1.Items.Clear();
            //if (button1.Text.ToString() != BtnType.ReRun.ToString())
            //    listBox1.Items.Insert(0, textBox6);
            textBox5.Clear();
            label7.Text = "00:00:00";
            label8.Text = "00:00:00";
            button1.Text = BtnType.Run.ToString();
            scriptDetails = new List<ScriptDetails>();
        }

        private void BtnReRun_Click()
        {
            Clear();

            AddFilesToList(sqlRun.filePaths, true);

            BtnRun_Click(true);
        }

        private void BtnStop_Click()
        {
            //if (listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Content.ToString().Contains("................Running")) != null)
            //{
            //    listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Content.ToString().Contains("................Running")).Foreground =
            //        Foreground = new SolidColorBrush(Colors.Blue);
            //    listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Content.ToString().Contains("................Running")).Content =
            //    listBox1.Items.OfType<ListBoxItem>().ToList().FirstOrDefault(x => x.Content.ToString().Contains("................Running")).Content.ToString().Replace("................Running", "................Stopped");
            //}

            sqlRun.StopProcessing();

            button1.Text = BtnType.Resume.ToString();
            button2.Enabled = true;
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            MyListBoxItem item = listBox1.Items[e.Index] as MyListBoxItem; // Get the current item and cast it to MyListBoxItem
            if (item != null)
            {
                //e.Graphics.DrawString( // Draw the appropriate text in the ListBox
                //    item.Message, // The message linked to the item
                //    listBox1.Font, // Take the font from the listbox
                //    new SolidBrush(item.ItemColor), // Set the color 
                //    0, // X pixel coordinate
                //    e.Index * listBox1.ItemHeight // Y pixel coordinate.  Multiply the index by the ItemHeight defined in the listbox.
                //);

                e.Graphics.DrawString( // Draw the appropriate text in the ListBox
                   item.Message, // The message linked to the item
                   listBox1.Font, // Take the font from the listbox
                   new SolidBrush(item.ItemColor), // Set the color 
                   e.Bounds,
                   StringFormat.GenericDefault
               );
            }
            else
            {
                // The item isn't a MyListBoxItem, do something about it
            }
        }
    }

    public class MyListBoxItem
    {
        public MyListBoxItem()
        {
        }

        public MyListBoxItem(Color c, string m)
        {
            ItemColor = c;
            Message = m;
        }
        public Color ItemColor { get; set; }
        public string Message { get; set; }
    }

}
